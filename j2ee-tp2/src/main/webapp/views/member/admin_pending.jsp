<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	isELIgnored="false" pageEncoding="ISO-8859-1"%>
<%@ include file="/views/import/html_structure_top.jspf"%>
<div class="row pin-msgn">
	<h2>Membres en attente</h2>
	<form method="POST" action="#">
		<table class="table">
			<c:forEach var="elements" items="${pending_members }">
				<tr>
					<th colspan="5">${elements.key.clubLabel }</th>
				</tr>
				<c:forEach var="member" items="${elements.value }">
					<tr>
						<td>${member.firstname}${member.lastname }</td>
						<td>${member.birthday }</td>
						<td>${member.email }</td>
						<td>${member.date_add }</td>
						<td>

							<button class="btn" type="submit" name="action"
								value="ref-${member.memberId }">

								<i style="color: #FF0000;" class="fa fa-remove"></i>
							</button>
							<button class="btn" type="submit" name="action"
								value="acc-${member.memberId }">

								<i style="color: #339933;" class="fa fa-check"></i>
							</button>

							<button class="btn" type="submit" name="action"
								value="adm-${member.clubId}-${member.memberId }">

								<i style="color: #FFFF00;" class="fa fa-star"></i>
							</button>
						</td>
					</tr>
				</c:forEach>
			</c:forEach>
		</table>
	</form>
</div>

<%@ include file="/views/import/html_structure_bottom.jspf"%>
