<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	isELIgnored="false" pageEncoding="ISO-8859-1"%>
<%@ include file="/views/import/html_structure_top.jspf"%>
<div class="row pin-msgn">
	<h2>Inscription</h2>
	<hr />
	<div class="col-lg-12">
		<form class="form-horizontal" method="POST" action="#">
			<c:if test="${err_inscr==1}">
				<p class="alert alert-danger">Tous les champs sont obligatoires
					!</p>
			</c:if>
			<c:if test="${err_inscr_email==1}">
				<p class="alert alert-danger">Format de l'email invalide !</p>
			</c:if>
			<c:if test="${err_inscr_date==1}">
				<p class="alert alert-danger">La date doit �tre au format Jour/mois/ann�e</p>
			</c:if>
			<div class="form-group">
				<div class="col-lg-4 col-md-4 col-sm-4">
					<label for="iEmail">Email</label>
				</div>
				<div class="col-lg-8  col-md-8 col-sm-8">
					<input name="email" class="form-control" id="iEmail"
						placeholder="votre adresse email" value="${email}" />

				</div>
			</div>
			<div class="form-group">
				<div class="col-lg-4 col-md-4 col-sm-4">
					<label for="iName">Nom</label>
				</div>
				<div class="col-lg-8  col-md-8 col-sm-8">
					<input name="lastname" class="form-control" placeholder="votre nom"
						id="iName" value="${lastname}" />

				</div>
			</div>
			<div class="form-group">
				<div class="col-lg-4 col-md-4 col-sm-4">
					<label for="ifname">Pr�nom</label>
				</div>
				<div class="col-lg-8  col-md-8 col-sm-8">
					<input name="firstname" value="${firstname}" class="form-control"
						placeholder="votre pr�nom" id="ifname" />

				</div>
			</div>
			<div class="form-group">
				<div class="col-lg-4 col-md-4 col-sm-4">
					<label for="dateOfBirth"> Date de naissance </label>
				</div>
				<div class="col-lg-8 col-md-8 col-sm-8">
					<input data-provide="datepicker" placeholder="jj/mm/aaaa"
						class="form-control" value="${dateOfBirth}" id="dateOfBirth"
						name="dateOfBirth" data-date-format="dd/mm/yyyy">
				</div>
			</div>
			<div class="form-group">
				<div class="col-lg-4 col-md-4 col-sm-4">
					<label for="sClub">Club</label>
				</div>
				<div class="col-lg-8  col-md-8 col-sm-8">

					<select class="form-control" id="sClub" name="club">
						<option value="0" selected="selected" disabled="disabled">choisir
							un club</option>
						<c:forEach var="club_list" items="${clubs}">

							<optgroup label="${club_list.key}">

								<c:forEach var="club" items="${club_list.value}">
									<option
										<c:if test="${clubid==club.clubId }"> selected="selected"</c:if>
										value="${club.clubId}">
										<c:out value="${club.clubLabel}" />
									</option>
								</c:forEach>
							</optgroup>
						</c:forEach>
					</select>
				</div>
			</div>
			<div class="form-group">
				<div class="col-lg-4 col-md-4 col-sm-4"></div>
				<div class="col-lg-8 col-md-8 col-sm-8">
					<input type="submit" value="s'inscrire" class="btn btn-success" />
				</div>
			</div>
		</form>
	</div>
</div>
<%@ include file="/views/import/html_structure_bottom.jspf"%>
