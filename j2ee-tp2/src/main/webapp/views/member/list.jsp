<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	isELIgnored="false" pageEncoding="ISO-8859-1"%>
<%@ include file="/views/import/html_structure_top.jspf"%>
<form method="POST" action="#">

	<div class="row pin-msgn">
		<h2>membres du club</h2>
		<table class="table table-bordered">

			<c:forEach var="member" items="${members }">
				<tr
					<c:if test="${ member.memberId== ref}">style='background-color:#99F;'</c:if>>

					<td>${member.firstname}${member.lastname }</td>
					<td>${member.birthday }</td>

					<td>${member.email }</td>
					<td>${member.date_add }</td>
					<td><c:if
							test="${ member.memberId!= ref && sessid==ref||adminid>0}">
							<button class="btn" type="submit" name="action"
								value="ref-${member.memberId }">

								<i style="color: #FF0000;" class="fa fa-remove"></i>
							</button>

						</c:if> <c:if test="${adminid>0&& member.memberId!= ref}">
							<button class="btn" type="submit" name="action"
								value="adm-${member.clubId}-${member.memberId }">

								<i style="color: #FFFF00;" class="fa fa-star"></i>
							</button>

						</c:if></td>

				</tr>
			</c:forEach>
		</table>
	</div>
	<c:if test="${ sessid== ref||adminid>0}">
		<div class="row pin-msgn">
			<h2>Membres en attente</h2>
			<table class="table table-bordered">
				<c:forEach var="member2" items="${pending_members }">
					<tr>
						<td>${member2.firstname}${member2.lastname }</td>
						<td>${member2.birthday }</td>

						<td>${member2.email }</td>
						<td>${member2.date_add }</td>
						<td><c:if
								test="${ member.memberId!= ref && sessid==ref ||adminid>0 }">
								<button class="btn" type="submit" name="action"
									value="acc-${member2.memberId }">
									<i style="color: #339933;" class="fa fa-check"></i>
								</button>

								<button class="btn" type="submit" name="action"
									value="ref-${member2.memberId }">
									<i style="color: #FF0000;" class="fa fa-remove"></i>
								</button>
							</c:if></td>

					</tr>
				</c:forEach>
			</table>
		</div>
		<div class="row pin-msgn">
			<h2>Membres refus�s</h2>
			<table class="table table-bordered">
				<c:forEach var="member2" items="${refused_members }">
					<tr>
						<td>${member2.firstname}${member2.lastname }</td>
						<td>${member2.birthday }</td>

						<td>${member2.email }</td>

						<td>${member2.date_add }</td>
						<td><c:if
								test="${ member.memberId!= ref && sessid==ref||adminid>0}">
								<button class="btn" type="submit" name="action"
									value="acc-${member2.memberId }">
									<i style="color: #339933;" class="fa fa-check"></i>
								</button>
							</c:if></td>

					</tr>
				</c:forEach>
			</table>
		</div>
	</c:if>
</form>
<%@ include file="/views/import/html_structure_bottom.jspf"%>
