<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	isELIgnored="false" pageEncoding="ISO-8859-1"%>
<%@ include file="/views/import/html_structure_top.jspf"%>
<div class="row pin-msgn">
	<h2>Inscription termin�e</h2>
	Votre demande d'inscription a bien �t� prise en compte. Vous recevrez
	un email lorsque votre inscription sera confirm�e.<br/><br/>
	Vous recevrez alors votre mot de passe pour vous connecter.
</div>

<%@ include file="/views/import/html_structure_bottom.jspf"%>
