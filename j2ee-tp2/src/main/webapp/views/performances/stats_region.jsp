<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	isELIgnored="false" pageEncoding="ISO-8859-1"%>
<%@ include file="/views/import/html_structure_top.jspf"%>
<div class="row pin-msgn">
	<div class="col-lg-12">
		<h2>Performances par r�gion</h2>
	</div>
</div>
<c:forEach var="perf" items="${stats }">
	<div class="row pin-msgn">
		<div class="col-lg-4 col-md-4 col-sm-6">
			<h4>${perf.key}<small>

					<br/>${perf.value.competition }</small>
					</h4>
		</div>
		<div class="col-lg-8 col-md-8 col-sm-6">
			<p>${perf.value.points}
				points <small>par ${perf.value.member_name } -
					${perf.value.club_name }</small>
			</p>
		</div>
	</div>
</c:forEach>

<%@ include file="/views/import/html_structure_bottom.jspf"%>