<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	isELIgnored="false" pageEncoding="ISO-8859-1"%>
<%@ include file="/views/import/html_structure_top.jspf"%>
<div class="row pin-msgn">
	<div class="col-lg-4 col-md-4 col-sm-4">
		<h2>Ajouter une performance</h2>
	</div>
	<div class="col-lg-8  col-md-8 col-sm-8">
		<form method="POST" action="#">
			<div class="form-group">
				<label> Type de performance</label> <select name="competition_id"
					class="form-control">
					<option value="0">-- Performance individuelle</option>

					<option value="-1">-- Cr�er une nouvelle comp�tition</option>
					<c:forEach var="competition" items="${competitions}">
						<option value="${competition.competition_id}"> ${competition.competitionlabel}</option>
					</c:forEach>
				</select>
				<script>
					$("select").change(function() {
						var i = $(this).val();
						if (i == -1) {
							$(".show_item").show();
						} else {
							$(".show_item").hide();
						}
					});
				</script>
				<div class="show_item" style="display: none;">
					<label>Cr�er une nouvelle comp�tition (laisser vide pour une performance individuelle) </label> <input type="text"
						placeholder="Nom de la nouvelle comp�tence"
						name="new_compet" class="form-control" />
				</div>
			</div>
			<div class="form-group">
				<label>Nombre de points</label> <input type="number" value="1"
					name="points" class="form-control" />
			</div>
			<div class="form-group">
				<input type="submit" value="Ajouter la performance"
					name="performance_submit" class="form-control" />
			</div>
		</form>
	</div>
</div>


<%@ include file="/views/import/html_structure_bottom.jspf"%>