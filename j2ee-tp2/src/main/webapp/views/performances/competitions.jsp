<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	isELIgnored="false" pageEncoding="ISO-8859-1"%>
<%@ include file="/views/import/html_structure_top.jspf"%>
<div class="row pin-msgn">
	<div class="col-lg-12">
		<h2>Selectionnez la compétition pour laquelle vous souhaitez des
			statistiques</h2>
		<ul class="nav nav-pills nav-stacked">
		<li><a
					href="<c:url value="/stats"/>">Meilleurs performances par région</a>
				</li>
			<li><a
					href="<c:url value="/stats"/>?cid=0">Performances libres</a>
				</li>
			<c:forEach var="compet" items="${competitions }">
				
				<li><a
					href="<c:url value="/stats"/>?cid=${compet.competition_id}">${compet.competitionlabel}</a>
				</li>
			</c:forEach>
		</ul>

	</div>
</div>


<%@ include file="/views/import/html_structure_bottom.jspf"%>