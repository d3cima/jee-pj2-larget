<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	isELIgnored="false" pageEncoding="ISO-8859-1"%>
<%@ include file="/views/import/html_structure_top.jspf"%>
<div class="row pin-msgn">
	<div class="col-lg-12">
		<h2>Liste des clubs disponibles sur le site</h2>
		<hr />

		<form method="GET">
			<div class="row">
				<div class="pull-right col-lg-3 col-md-3 col-sm-3">
					<select class="form-control" name="sort" onChange="submit()">
						<option>trier par...</option>
						<option value="region">region</option>


						<option value="alpha">alphabet</option>
					</select>
				</div>
			</div>
		</form>
		<c:forEach var="club_list" items="${clubs}">
			<h3>${club_list.key}</h3>
			<ul class="nav nav-pills nav-stacked	">
				<c:forEach var="club" items="${club_list.value}">
					<c:choose>
						<c:when test="${adminid>0}">
							<li role="presentation"><a
								href="<c:url value="/members?cid=${club.clubId }"/>">${club.clubLabel}</a></li>
						</c:when>
						<c:otherwise>
							<li role="presentation"><c:out value="${club.clubLabel}" /></li>

						</c:otherwise>
					</c:choose>


					</a>
					</li>
				</c:forEach>
			</ul>
		</c:forEach>
	</div>
</div>


<%@ include file="/views/import/html_structure_bottom.jspf"%>

