package fr.larget.hibernate;

import java.io.Serializable;
import java.util.List;

import javax.transaction.Transaction;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
/**
 * Appels � la base et manipulation des competitions
 * @author Henri L.
 *
 */
public class CompetitionsManager {
	static final Logger logger = LoggerFactory
			.getLogger(CompetitionsManager.class);

	public CompetitionsManager() {

	}

	public static int addCompetition(String competitionName) {
		Session session = HibernateSessionManager.getSessionFactory()
				.openSession();
		session.beginTransaction();

		Competitions comp = new Competitions();

		comp.setCompetitionlabel(competitionName);
		Serializable s = session.save(comp);
		session.getTransaction().commit();
		session.close();
		try {

			if (s != null) {
				return (Integer) s;
			}
		} catch (Exception e) {
			// nothing to do, just return 0.
		}
		return 0;

	}

	public static List<Competitions> listCompetitions() {
		Session session = HibernateSessionManager.getSessionFactory()
				.openSession();
		org.hibernate.Transaction tx = session.beginTransaction();
		List<Competitions> competitions = session.createCriteria(
				Competitions.class).list();
		return competitions;
	}

	public static String getCompetitionName(int cid) {
		if (cid < 1) {
			return "Performance individuelle";
		}
		Session session = HibernateSessionManager.getSessionFactory()
				.openSession();
		org.hibernate.Transaction tx = session.beginTransaction();
		List<Competitions> comp = session.createCriteria(Competitions.class)
				.add(Restrictions.eq("competition_id", cid)).list();
		if (comp.size() > 0) {
			Competitions c = comp.get(0);
			return c.getCompetitionlabel();
		}
		
		return "Performance individuelle";
	}
}
