package fr.larget.hibernate;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
/**
 * entit้ performances
 * @author Henri L.
 *
 */
@Entity
@Table(name = "performance")
public class Performances implements Serializable {


	@Override
	public String toString() {
		return "Performances [performance_id=" + performance_id
				+ ", competition_id=" + competition_id + ", member_id="
				+ member_id + ", points=" + points + "]";
	}

	@Id
	@GeneratedValue
	@Column(name = "pid")
	private int performance_id;

	@Column(name = "compid")
	private int competition_id;
	
	@Column(name = "mid")
	private int member_id;
	
	@Column(name="points")
	private int points;

	public int getPerformance_id() {
		return performance_id;
	}

	public void setPerformance_id(int performance_id) {
		this.performance_id = performance_id;
	}

	public int getCompetition_id() {
		return competition_id;
	}

	public void setCompetition_id(int competition_id) {
		this.competition_id = competition_id;
	}

	public int getMember_id() {
		return member_id;
	}

	public void setMember_id(int member_id) {
		this.member_id = member_id;
	}

	public int getPoints() {
		return points;
	}

	public void setPoints(int points) {
		this.points = points;
	}


}