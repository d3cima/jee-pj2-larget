package fr.larget.hibernate;

import java.util.List;

import javax.transaction.Transaction;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.client.RestTemplate;
/**
 * Appels � la base et manipulation des clubs
 * @author Henri L.
 *
 */
public class ClubsManager {
	static final Logger logger = LoggerFactory.getLogger(ClubsManager.class);

	public ClubsManager() {

	}

	public void addClub(String clubLabel, String region, Members member) {
		Session session = HibernateSessionManager.getSessionFactory()
				.openSession();
		session.beginTransaction();

		Clubs club = new Clubs();
		club.setClubLabel(clubLabel);

		session.save(club);
		session.getTransaction().commit();
		session.close();
	}

	public static List<Clubs> listClubs() {
		Session session = HibernateSessionManager.getSessionFactory()
				.openSession();
		org.hibernate.Transaction tx = session.beginTransaction();
		List<Clubs> clubs = session.createCriteria(Clubs.class).list();
		return clubs;
	}

	public void deleteClub(String clubLabel) {
		Session session = HibernateSessionManager.getSessionFactory()
				.openSession();
		session.beginTransaction();

		Criteria criteria = session.createCriteria(Clubs.class);
		criteria.add(Restrictions.eq("clubLabel", clubLabel));
		Clubs club = (Clubs) criteria.uniqueResult();

		session.delete(club);
		session.getTransaction().commit();
		session.close();
	}

	public static String getClubName(int cid) {
		Session session = HibernateSessionManager.getSessionFactory()
				.openSession();
		org.hibernate.Transaction tx = session.beginTransaction();
		List<Clubs> clubs = session.createCriteria(Clubs.class)
				.add(Restrictions.eq("clubId", cid)).list();
		session.close();
		if (clubs.size() > 0) {
			Clubs c = clubs.get(0);
			return c.getClubLabel();
		}
		return "pas de club";
	}

	public static int getReferentId(int cid) {
		Session session = HibernateSessionManager.getSessionFactory()
				.openSession();
		org.hibernate.Transaction tx = session.beginTransaction();
		List<Clubs> clubs = session.createCriteria(Clubs.class)
				.add(Restrictions.eq("clubId", cid)).list();
		session.close();
		if (clubs.size() > 0) {
			
			
			Clubs c = clubs.get(0);
			return c.getReferent();
		}
		return 0;
	}

	public static void updateClub(Clubs c) {
		Session session = HibernateSessionManager.getSessionFactory()
				.openSession();
		session.beginTransaction();
		session.update(c);
		session.getTransaction().commit();
		session.close();
	}

	public static void setReferent(int cid, int mid) {
		Session session = HibernateSessionManager.getSessionFactory()
				.openSession();
		org.hibernate.Transaction tx = session.beginTransaction();
		List<Clubs> clubs = session.createCriteria(Clubs.class)
				.add(Restrictions.eq("clubId", cid)).list();
		session.close();
		if (clubs.size() > 0) {
			Clubs c = clubs.get(0);
			c.setReferent(mid);
			ClubsManager.updateClub(c);
		}
	}

}
