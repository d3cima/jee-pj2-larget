package fr.larget.hibernate;

import javax.persistence.*;

import fr.larget.utils.exceptions.EmailSyntaxException;

import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Date;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.List;
import java.util.Random;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * ENtit� member
 * @author Henri L.
 *
 */
@Entity
@Table(name = "member")
public class Members implements Serializable {
	private static final String SALT_KEY = "ZKXQaTwV5s8PDmr6zCxA2YmHPyngTlUkiLKLlvsI";
	@Id
	@GeneratedValue
	@Column(name = "mid")
	private int memberId;

	@Column(name = "firstname")
	private String firstname;

	@Column(name = "lastname")
	private String lastname;

	@Column(name = "email")
	private String email;

	@Column(name = "password")
	private String password = null;

	@Column(name = "date_of_birth")
	private String date_of_birth;

	@Column(name = "cid")
	private int clubId;

	@Column(name = "accepted")
	private int is_accepted = 0;
	@Column(name = "date_add")
	private String date_add;

	public String getDate_add() {
		return date_add;
	}

	public void setDate_add(String date_add) {
		this.date_add = date_add;
	}

	public Members() {

	}

	public int getMemberId() {
		return memberId;
	}

	public void setMemberId(int memberId) {
		this.memberId = memberId;
	}

	public String getFirstname() {
		return firstname;
	}

	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}

	public String getLastname() {
		return lastname;
	}

	public void setLastname(String lastname) {
		this.lastname = lastname;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) throws EmailSyntaxException {
		if (this.checkmail(email)) {
			this.email = email;
		} else {
			throw new EmailSyntaxException();

		}

	}

	public static boolean checkmail(String email) {
		Pattern p = Pattern.compile("^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
				+ "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$");
		Matcher m = p.matcher(email);
		return m.matches();

	}

	public String getPassword() {
		return password;
	}

	public String generatePassword() {
		String alphas = this.shuffleString("abcdefghijklmnopqrstuvwxyz");
		String majAlphas = this.shuffleString("ABCDEFGHIJKLMNOPQRSTUVWXYZ");
		String numbers = this.shuffleString("0123456789");
		String specials = this.shuffleString("#[]():;,&@?%!*-+");
		String new_password = "FailedGeneratingPassword";

		new_password = alphas.substring(0, new Random().nextInt(2) + 2);
		new_password = new_password.concat(majAlphas.substring(0,
				new Random().nextInt(1) + 1));
		new_password = new_password.concat(numbers.substring(0,
				new Random().nextInt(4) + 4));
		new_password = new_password.concat(specials.substring(0,
				new Random().nextInt(1) + 1));

		return this.shuffleString(new_password);
	}

	private String shuffleString(String string) {
		List<String> letters = Arrays.asList(string.split(""));
		Collections.shuffle(letters);
		String shuffled = "";
		for (String letter : letters) {
			shuffled += letter;
		}
		return shuffled;
	}

	public static String hashPassword(String pwd) {
		String md5 = null;

		if (null == pwd)
			return null;

		try {

			// Create MessageDigest object for MD5
			MessageDigest digest = MessageDigest.getInstance("MD5");

			// Update input string in message digest
			digest.update(pwd.getBytes(), 0, pwd.length());

			// Converts message digest value in base 16 (hex)
			md5 = new BigInteger(1, digest.digest()).toString(16);
			return md5;

		} catch (NoSuchAlgorithmException e) {

		}
		return pwd;

	}

	public void setPassword(String password) {

		this.password = Members.hashPassword(password);

	}

	public String getBirthday() {
		String date[] = this.date_of_birth.split("-");
		
		return date[2] + "/" + date[1] + "/" + date[0];
	}

	public Calendar getDate_of_birth() throws ParseException {
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		Calendar c = Calendar.getInstance();
		c.setTime(formatter.parse(this.date_of_birth));
		return c;

	}

	public void setDate_of_birth(Calendar c) {

		this.date_of_birth = new SimpleDateFormat("yyyy-MM-dd").format(c
				.getTime());
		System.out.println(this.date_of_birth);
	}

	public void setDate_of_birth(int y, int m, int d) {
		Calendar cal = Calendar.getInstance();
		cal.set(y, m - 1, d);
		this.setDate_of_birth(cal);
	}

	public int getClubId() {
		return clubId;
	}

	public void setClubId(int clubId) {
		this.clubId = clubId;
	}

	public Boolean is_accepted() {
		return is_accepted == 1;
	}

	public Boolean is_refused() {
		return is_accepted == -1;
	}

	public void accept() {
		this.is_accepted = 1;
	}

	public void refuse() {
		this.is_accepted = -1;
	}

	@Override
	public String toString() {
		return "Members [memberId=" + memberId + ", firstname=" + firstname
				+ ", lastname=" + lastname + ", email=" + email + ", password="
				+ password + ", date_of_birth=" + date_of_birth + ", clubId="
				+ clubId + ", is_accepted=" + is_accepted + "]";
	}

}