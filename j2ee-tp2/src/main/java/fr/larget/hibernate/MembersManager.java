package fr.larget.hibernate;

import java.util.HashMap;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.criterion.CriteriaQuery;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.hibernate.engine.spi.TypedValue;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.larget.utils.HTMLMail;
import fr.larget.hibernate.Members;
/**
 * Appels � la base et manipulation des membres
 * @author Henri L.
 *
 */
public class MembersManager {
	static final Logger logger = LoggerFactory.getLogger(MembersManager.class);

	public static HashMap<Clubs, List<Members>> getAllMembersByRegion(
			int pending) {
		HashMap h = new HashMap<Clubs, List<Members>>();
		List<Clubs> clubs = ClubsManager.listClubs();
		for (int i = 0; i < clubs.size(); i++) {
			Clubs c = clubs.get(i);
			List<Members> m = MembersManager.getMembers(c.getClubId(), pending);
			if (m.size() > 0)
				h.put(c, m);
		}
		return h;

	}

	public static List<Members> getMembers(int i, int pending) {
		Session session = HibernateSessionManager.getSessionFactory()
				.openSession();
		org.hibernate.Transaction tx = session.beginTransaction();
		Criterion c1, c2;
		c1 = Restrictions.eq("clubId", i);

		c2 = Restrictions.eq("is_accepted", pending);
		List<Members> m;
		try {
			m = session.createCriteria(Members.class).add(c1).add(c2)
					.addOrder(Order.desc("date_add")).list();

		} catch (IndexOutOfBoundsException e) {
			m = null;
		}
		session.close();
		return m;

	}

	public static void addMember(Members m) {
		Session session = HibernateSessionManager.getSessionFactory()
				.openSession();
		session.beginTransaction();

		session.save(m);
		session.getTransaction().commit();
		session.close();
	}

	public static void updateMember(Members m) {
		Session session = HibernateSessionManager.getSessionFactory()
				.openSession();
		session.beginTransaction();

		session.update(m);
		session.getTransaction().commit();
		session.close();
	}

	public static int login(String email, String password) {
		Session session = HibernateSessionManager.getSessionFactory()
				.openSession();
		org.hibernate.Transaction tx = session.beginTransaction();
		Criterion c1, c2, c3;
		c1 = Restrictions.eq("email", email.trim().toLowerCase());
		c2 = Restrictions.eq("password", Members.hashPassword(password.trim()));
		c3 = Restrictions.eq("is_accepted", 1);
		try {
			Members m = (Members) session.createCriteria(Members.class).add(c1)
					.add(c2).add(c3).list().get(0);

			System.out.println(m);
			session.close();
			return m.getMemberId();

		} catch (IndexOutOfBoundsException e) {
			session.close();
			return 0;
		}

	}

	public static void refuseMember(int memberid) {
		Session session = HibernateSessionManager.getSessionFactory()
				.openSession();
		org.hibernate.Transaction tx = session.beginTransaction();
		Criterion c1;
		c1 = Restrictions.eq("memberId", memberid);
		try {
			Members m = (Members) session.createCriteria(Members.class).add(c1)
					.list().get(0);
			session.close();
			m.refuse();
			MembersManager.updateMember(m);

		} catch (IndexOutOfBoundsException e) {
		}
	}

	public static void acceptMember(int memberid) {
		Session session = HibernateSessionManager.getSessionFactory()
				.openSession();
		org.hibernate.Transaction tx = session.beginTransaction();
		Criterion c1;
		c1 = Restrictions.eq("memberId", memberid);
		try {
			Members m = (Members) session.createCriteria(Members.class).add(c1)
					.list().get(0);
			String pass = m.generatePassword();
			session.close();

			m.setPassword(pass);
			m.accept();
			MembersManager.updateMember(m);
			HTMLMail.sendHTML(
					m.getEmail(),
					"Votre inscription a �t� valid�e",
					"<h2>Bonjour,</h2>"
							+ "Votre inscription � <b>SPORTASSOC</b> a bien �t� valid�e, votre mot de passe pour vous connecter est le suivant : <br/>"
							+ "<center><b style='text-align:center;font-famil:\"Lucida Console\", Monaco, monospace;'>"
							+ pass
							+ "</b></center><br/><br/>"
							+ "Vous pouvez d�s � pr�sent vous connecter est acc�der � l'ensemble des services de SportAssoc."
							+ "<br/><br/>L'�quipe SportAssoc");
		} catch (IndexOutOfBoundsException e) {
		}
	}

	public static Members getMember(int member_id) {
		Session session = HibernateSessionManager.getSessionFactory()
				.openSession();
		org.hibernate.Transaction tx = session.beginTransaction();
		List<Members> comp = session.createCriteria(Members.class)
				.add(Restrictions.eq("memberId", member_id)).list();
		session.close();
		if (comp.size() > 0) {
			Members c = comp.get(0);
			return c;
		}
		return null;
	}

}
