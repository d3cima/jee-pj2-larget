package fr.larget.hibernate;

import javax.persistence.*;
import java.io.Serializable;
/**
 * Entit� club
 * @author Henri L.
 *
 */
@Entity
@Table(name = "club")
public class Clubs implements Serializable, Comparable<Clubs> {

	@Id
	@GeneratedValue
	@Column(name = "cid")
	private int clubId;

	@Column(name = "club_name")
	private String clubLabel;

	@Column(name = "region")
	private String region;

	@Column(name = "referentid")
	private int member = 0;

	public Clubs() {

	}

	public String getRegion() {
		return region;
	}

	public void setRegion(String region) {
		this.region = region;
	}

	public int getClubId() {
		return clubId;
	}

	public void setClubId(int clubId) {
		this.clubId = clubId;
	}

	public String getClubLabel() {
		return clubLabel;
	}

	public void setClubLabel(String clubLabel) {
		this.clubLabel = clubLabel;
	}

	public int getReferent() {
		return this.member;
	}

	public void setReferent(int member) {
		this.member = member;
	}

	@Override
	public String toString() {
		return "Clubs [clubId=" + clubId + ", clubLabel=" + clubLabel
				+ ", region=" + region + "]";
	}

	public int compareTo(Clubs o) {
		return this.getClubLabel().compareTo(o.getClubLabel()) ;
	}

}