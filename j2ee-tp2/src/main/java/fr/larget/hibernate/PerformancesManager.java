package fr.larget.hibernate;

import java.util.ArrayList;
import java.util.List;

import javax.transaction.Transaction;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
/**
 * Appels � la base et manipulation des performances
 * @author Henri L.
 *
 */
public class PerformancesManager {
	static final Logger logger = LoggerFactory
			.getLogger(PerformancesManager.class);

	public PerformancesManager() {

	}

	public static void addPerformance(Performances p) {
		Session session = HibernateSessionManager.getSessionFactory()
				.openSession();
		session.beginTransaction();
		session.save(p);
		session.getTransaction().commit();
		session.close();
	}

	public static List<Performances> getPerformancesByRegion(String regionName) {
		String sql = "SELECT p.* from performance p join member m on p.mid=m.mid join club c on m.cid=c.cid where lower(c.region)= :region  ORDER BY points DESC";
		Session session = HibernateSessionManager.getSessionFactory()
				.openSession();
		session.beginTransaction();

		Query query = session.createSQLQuery(sql).setParameter("region",
				regionName.toLowerCase());
		List result = query.list();
		session.close();
		List<Performances> list = new ArrayList<>();
		for (int i = 0; i < result.size(); i++) {

			Object[] obj = (Object[]) result.get(i);
			Performances p = new Performances();
			p.setPerformance_id((int) obj[0]);
			p.setCompetition_id((int) obj[1]);
			p.setMember_id((int) obj[2]);
			p.setPoints((int) obj[3]);
			list.add(p);
		}

		return list;
	}

	public static List<Performances> getPerformancesByRegionAndCompetition(
			String regionName, int competitionid) {
		String sql = "SELECT p.* from performance p join member m on p.mid=m.mid join club c on m.cid=c.cid where lower(c.region)= :region AND p.compid= :competition ORDER BY points DESC";
		Session session = HibernateSessionManager.getSessionFactory()
				.openSession();
		session.beginTransaction();

		Query query = session.createSQLQuery(sql)
				.setParameter("region", regionName.toLowerCase())
				.setParameter("competition", competitionid);
		List result = query.list();
		session.close();
		List<Performances> list = new ArrayList<>();
		for (int i = 0; i < result.size(); i++) {

			Object[] obj = (Object[]) result.get(i);
			Performances p = new Performances();
			p.setPerformance_id((int) obj[0]);
			p.setCompetition_id((int) obj[1]);
			p.setMember_id((int) obj[2]);
			p.setPoints((int) obj[3]);
			list.add(p);
		}

		return list;
	}

}
