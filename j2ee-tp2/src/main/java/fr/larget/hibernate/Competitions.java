package fr.larget.hibernate;

import javax.persistence.*;
import java.io.Serializable;
/**
 * Entit� de competitions
 * @author Henri L.
 *
 */
@Entity
@Table(name = "competition")
public class Competitions implements Serializable {


	@Id
	@GeneratedValue
	@Column(name = "compid")
	private int competition_id;

	@Column(name = "competition_label")
	private String competitionlabel;

	public int getCompetition_id() {
		return competition_id;
	}

	public void setCompetition_id(int competition_id) {
		this.competition_id = competition_id;
	}

	public String getCompetitionlabel() {
		return competitionlabel;
	}

	public void setCompetitionlabel(String competitionlabel) {
		this.competitionlabel = competitionlabel;
	}



}