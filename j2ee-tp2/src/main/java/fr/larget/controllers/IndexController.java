package fr.larget.controllers;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import fr.larget.hibernate.Clubs;
import fr.larget.hibernate.ClubsManager;
import fr.larget.hibernate.Members;
import fr.larget.hibernate.MembersManager;
import fr.larget.utils.exceptions.EmailSyntaxException;
/**
 * Page principale du site
 * @author Henri L.<h.larget@gmail.com>
 *
 */
@Controller
public class IndexController {


	@RequestMapping("/")
	public String mainPage(final ModelMap pModel,HttpSession sessionObj) {
		pModel.addAttribute("viewtitle", "Sportassoc - �changez - �voluez ");
		try{
		pModel.addAttribute("sessid",sessionObj.getAttribute("uid").toString());
		}catch(NullPointerException e){
			pModel.addAttribute("sessid",0);
		}
		try{
			pModel.addAttribute("adminid",sessionObj.getAttribute("adminid").toString());
			}catch(NullPointerException e){
				pModel.addAttribute("adminid",0);
			}
		return "index.jsp";
	}

}
