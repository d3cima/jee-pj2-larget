package fr.larget.controllers;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.session.Session;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import fr.larget.hibernate.Clubs;
import fr.larget.hibernate.ClubsManager;
import fr.larget.hibernate.Members;
import fr.larget.hibernate.MembersManager;
import fr.larget.utils.exceptions.EmailSyntaxException;
/**
 * connexion, déconnexion, inscription, listing des membres
 * @author Henri L
 *
 */
@Controller
public class MemberController {

	@RequestMapping(value = "/login", method = RequestMethod.POST)
	public String login(
			final ModelMap pModel,
			HttpSession sessionObj,
			@RequestParam(value = "email", required = false) final String email,
			@RequestParam(value = "password", required = false) final String password) {

		int res = MembersManager.login(email, password);
		if (res > 0) {

			sessionObj.setAttribute("uid", res);

			return "redirect:/";

		} else {
			return "member/bad_login.jsp";
		}
	}

	@RequestMapping(value = "/logout")
	public String logout(final ModelMap pModel, HttpSession sessionObj) {
		sessionObj.removeAttribute("uid");
		sessionObj.removeAttribute("adminid");
		return "redirect:/";
	}

	@RequestMapping(value = "/signin", method = RequestMethod.GET)
	public String signin(final ModelMap pModel, HttpSession sessionObj) {

		if ((String) sessionObj.getAttribute("uid") != null) {
			return "redirect:/";
		}
		pModel.addAttribute("clubs", ClubController.getSorted("region"));
		return "member/inscription.jsp";
	}

	@RequestMapping(value = "/signin", method = RequestMethod.POST)
	public String signin_treatment(
			final ModelMap pModel,
			HttpSession sessionObj,
			@RequestParam(value = "email", required = false) final String email,
			@RequestParam(value = "firstname", required = false) final String firstname,
			@RequestParam(value = "lastname", required = false) final String lastname,
			@RequestParam(value = "dateOfBirth", required = false) final String date_of_birth,
			@RequestParam(value = "club", required = false) final String club) {

		pModel.addAttribute("email", email);

		pModel.addAttribute("email", email);
		pModel.addAttribute("firstname", firstname);
		pModel.addAttribute("lastname", lastname);
		pModel.addAttribute("dateOfBirth", date_of_birth);
		pModel.addAttribute("clubid", club);

		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		boolean date_ok = false;
		try {
			Date date = sdf.parse(date_of_birth.trim());
			date_ok = true;
		} catch (ParseException e) {
			// nothing to do, date_ok already to false.
		}

		sdf.setLenient(false);

		if (email.trim() == "" || firstname.trim() == ""
				|| lastname.trim() == "" || date_of_birth.trim() == ""
				|| club == null) {
			pModel.addAttribute("err_inscr", 1);
		} else if (!date_ok) {
			pModel.addAttribute("err_inscr_date", 1);

		} else {
			try {
				Members m = new Members();
				String[] a = date_of_birth.trim().split("/");
				m.setDate_of_birth(Integer.parseInt(a[2]),
						Integer.parseInt(a[1]), Integer.parseInt(a[0]));
				m.setEmail(email.trim().toLowerCase());
				m.setFirstname(firstname.trim());
				m.setLastname(lastname.trim());
				m.setClubId(Integer.parseInt(club));

				MembersManager me = new MembersManager();
				try {
					me.addMember(m);
				} catch (Exception e) {

				}
				return "member/confirmation_inscription.jsp";
			} catch (EmailSyntaxException e) {
				pModel.addAttribute("err_inscr_email", 1);
			}

		}

		return this.signin(pModel, sessionObj);
	}

	@RequestMapping(value = "/members", method = RequestMethod.GET)
	public String members(final ModelMap pModel, HttpSession sessionObj,
			@RequestParam(value = "cid", required = false) String clubid) {
		boolean ok = false;
		try {
			Integer admini = (Integer) sessionObj.getAttribute("adminid");
			if (admini != null) {
				ok = true;
			}

		} catch (Exception e) {

			ok = false;
		}

		if (sessionObj.getAttribute("uid") == null && !ok) {
			return "redirect:/";
		}
		if (sessionObj.getAttribute("uid") != null) {
			pModel.addAttribute("sessid", sessionObj.getAttribute("uid"));
		}
		int mid = 0;

		mid = (int) sessionObj.getAttribute("uid");
		if (mid < 1 && !ok) {
			return "redirect:/";
		} else {
			int ref;
			int club_id = 0;
			Members member = new Members();
			if (!ok) {
				member = MembersManager.getMember((mid));

				pModel.addAttribute("sessid", mid);
				club_id = member.getClubId();
			} else {
				try {
					club_id = Integer.parseInt(clubid);
				} catch (Exception e) {
					member = MembersManager.getMember((mid));

					pModel.addAttribute("sessid", mid);
					club_id = member.getClubId();
				}
			}
			ref = ClubsManager.getReferentId(club_id);
			MembersManager m = new MembersManager();

			pModel.addAttribute("members", m.getMembers(club_id, 1));
			pModel.addAttribute("ref", ref);
			if (ref == member.getMemberId() || ok) {
				pModel.addAttribute("pending_members", m.getMembers(club_id, 0));
				pModel.addAttribute("refused_members",
						m.getMembers(club_id, -1));
			}
			try {
				pModel.addAttribute("adminid",
						sessionObj.getAttribute("adminid").toString());
			} catch (NullPointerException e) {
				pModel.addAttribute("adminid", 0);
			}
			return "member/list.jsp";
		}
	}

	@RequestMapping(value = "/members", method = RequestMethod.POST)
	public String member_accept(final ModelMap pModel, HttpSession sessionObj,
			@RequestParam(value = "cid", required = false) String clubid,
			@RequestParam(value = "action", required = false) final String act) {
		String[] action = act.split("-");

		if (action[0].equalsIgnoreCase("ref")) {

			MembersManager.refuseMember(Integer.parseInt(action[1]));
		} else if (action[0].equalsIgnoreCase("acc")) {
			MembersManager.acceptMember(Integer.parseInt(action[1]));

		} else if (action[0].equalsIgnoreCase("adm")) {
			ClubsManager.setReferent(Integer.parseInt(action[1]),
					Integer.parseInt(action[2]));

		}
		return this.members(pModel, sessionObj, clubid);

	}
}
