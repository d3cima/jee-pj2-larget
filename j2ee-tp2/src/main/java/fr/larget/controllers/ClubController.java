package fr.larget.controllers;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import fr.larget.hibernate.Clubs;
import fr.larget.hibernate.ClubsManager;
/**
 * Gestion des clubs/listing
 * @author Henri L.
 *
 */
@Controller
public class ClubController {

	@RequestMapping(value = "/clubs", method = RequestMethod.GET)
	public String list_of_clubs(final ModelMap pModel, HttpSession sessionObj,
			@RequestParam(value = "sort", required = false) final String sort) {

		HashMap<String, List<Clubs>> res = getSorted(sort);
		String sval = sort == null ? "region" : sort;
		pModel.addAttribute("sort_criteria", sval);
		pModel.addAttribute("viewtitle", "Liste des clubs sportifs");
		pModel.addAttribute("clubs", res);

		try {
			pModel.addAttribute("sessid", sessionObj.getAttribute("uid")
					.toString());

		} catch (NullPointerException e) {
			pModel.addAttribute("sessid", 0);

		}
		try {
			pModel.addAttribute("adminid", sessionObj.getAttribute("adminid")
					.toString());

		} catch (NullPointerException e) {
			pModel.addAttribute("adminid", 0);

		}
		return "clubs.jsp";
	}

	public static HashMap<String, List<Clubs>> getSorted(String sort) {
		ClubsManager cm = new ClubsManager();
		HashMap<String, List<Clubs>> res = new HashMap<String, List<Clubs>>();
		String key = "";
		List<Clubs> c;
		List<Clubs> d;
		String sval = sort;
		if (sval == null)
			sval = "region";
		for (Clubs i : cm.listClubs()) {
			if (sval.equals("alpha")) {
				key = i.getClubLabel().substring(0, 1).toUpperCase();
			} else {
				key = i.getRegion().toUpperCase();
			}

			if (!res.containsKey(key)) {
				c = new ArrayList<Clubs>();
				res.put(key, c);
			}
			d = res.get(key);
			d.add(i);
			Collections.sort(d);
			res.put(key, d);

		}
		return res;
	}

}
