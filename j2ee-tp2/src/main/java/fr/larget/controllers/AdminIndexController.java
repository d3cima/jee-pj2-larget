package fr.larget.controllers;

import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.aop.interceptor.PerformanceMonitorInterceptor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import fr.larget.hibernate.Clubs;
import fr.larget.hibernate.CompetitionsManager;
import fr.larget.hibernate.MembersManager;
import fr.larget.hibernate.Performances;
import fr.larget.hibernate.PerformancesManager;
import fr.larget.utils.HTMLMail;
/**
 * G�rer la connexion � l'administration ainsi que les membres en attente
 * @author Henri L. <h.larget@gmail.com>
 *
 */
@Controller
public class AdminIndexController {
	@RequestMapping(value = "/admin", method = RequestMethod.GET)
	public String admin_home(final ModelMap pModel, HttpSession sessionObj) {
		try {
			pModel.addAttribute("sessid", sessionObj.getAttribute("uid")
					.toString());
		} catch (NullPointerException e) {
			pModel.addAttribute("sessid", 0);
		}
		try {
			pModel.addAttribute("adminid", sessionObj.getAttribute("adminid")
					.toString());
		} catch (NullPointerException e) {
			pModel.addAttribute("adminid", 0);
		}
		return "admin_login.jsp";
	}

	@RequestMapping(value = "/admin", method = RequestMethod.POST)
	public String admin_home_process(
			final ModelMap pModel,
			HttpSession sessionObj,
			@RequestParam(value = "password", required = false) final String password) {
		/* MDP en brute dans le code pour le moment */
		if (password.equals("pz33Qthe")) {
			sessionObj.setAttribute("adminid", 1);
		} else {
			return this.admin_home(pModel, sessionObj);
		}

		return "redirect:/";

	}

	@RequestMapping(value = "/admin_pending", method = RequestMethod.GET)
	public String admin_pending_requests(final ModelMap pModel,
			HttpSession sessionObj) {
		int adminid = 0;
		try {
			System.out.println("ok");
			adminid = (int) sessionObj.getAttribute("adminid");
		} catch (Exception e) {
			return "redirect:/";
		}
		if (adminid < 1) {
			return "redirect:/";
		}

		MembersManager m = new MembersManager();
		try {
			pModel.addAttribute("sessid", sessionObj.getAttribute("uid")
					.toString());
		} catch (NullPointerException e) {
			pModel.addAttribute("sessid", 0);
		}
		try {
			pModel.addAttribute("adminid", sessionObj.getAttribute("adminid")
					.toString());
		} catch (NullPointerException e) {
			pModel.addAttribute("adminid", 0);
		}
		pModel.addAttribute("pending_members", m.getAllMembersByRegion(0));
		return "member/admin_pending.jsp";

	}

	@RequestMapping(value = "/admin_pending", method = RequestMethod.POST)
	public String admin_pending_requests_edit(final ModelMap pModel,
			HttpSession sessionObj,
			@RequestParam(value = "action", required = false) final String act) {

		MemberController m = new MemberController();
		m.member_accept(pModel, sessionObj, "1", act);
		return this.admin_pending_requests(pModel, sessionObj);

	}
}
