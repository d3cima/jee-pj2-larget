package fr.larget.controllers;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Set;

import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import fr.larget.hibernate.Clubs;
import fr.larget.hibernate.ClubsManager;
import fr.larget.hibernate.Competitions;
import fr.larget.hibernate.CompetitionsManager;
import fr.larget.hibernate.Members;
import fr.larget.hibernate.MembersManager;
import fr.larget.hibernate.Performances;
import fr.larget.hibernate.PerformancesManager;
import fr.larget.utils.VisualPerformance;
/**
 * Enregistrement des performances et consultation
 * @author Henri L.
 *
 */
@Controller
public class CompetitionPerformanceController {

	@RequestMapping(value = "/nouvelle_performance", method = RequestMethod.GET)
	public String ajouterPerformance(final ModelMap pModel,
			HttpSession sessionObj) {
		if (sessionObj.getAttribute("uid").toString() == null) {
			return "redirect:/";
		}
		pModel.addAttribute("competitions",
				CompetitionsManager.listCompetitions());

		pModel.addAttribute("sessid", sessionObj.getAttribute("uid").toString());

		return "performances/add.jsp";

	}

	@RequestMapping(value = "/nouvelle_performance", method = RequestMethod.POST)
	public String ajouterPerformance_form(
			final ModelMap pModel,
			HttpSession sessionObj,
			@RequestParam(value = "competition_id", required = false) final String competition_id,
			@RequestParam(value = "new_compet", required = false) final String new_comp,
			@RequestParam(value = "points", required = false) final String points) {
		int ipts = 0;
		try {
			ipts = Integer.parseInt(points);
		} catch (Exception e) {
			return ajouterPerformance(pModel, sessionObj);
		}
		if (sessionObj.getAttribute("uid").toString() == null) {
			return "redirect:/";
		}

		int iCid = Integer.parseInt(competition_id);
		if (iCid < 0) {
			if (new_comp.length() > 0) {
				iCid = CompetitionsManager.addCompetition(new_comp);
			} else {
				iCid = 0;
			}
		}

		Performances p = new Performances();

		p.setCompetition_id(iCid);
		p.setMember_id(Integer.parseInt(sessionObj.getAttribute("uid")
				.toString()));
		p.setPoints(ipts);

		PerformancesManager.addPerformance(p);
		pModel.addAttribute("sessid", sessionObj.getAttribute("uid").toString());
		try{
			pModel.addAttribute("adminid",sessionObj.getAttribute("adminid").toString());
			}catch(NullPointerException e){
				pModel.addAttribute("adminid",0);
			}
		return "performances/add_success.jsp";
	}

	@RequestMapping(value = "/competitions", method = RequestMethod.GET)
	public String competitions(final ModelMap pModel, HttpSession sessionObj) {
		List<Competitions> comps = CompetitionsManager.listCompetitions();

		pModel.addAttribute("competitions", comps);
		try {
			pModel.addAttribute("sessid", sessionObj.getAttribute("uid")
					.toString());
		} catch (Exception e) {
			// nothing to do, user not connected.
		}
		try{
			pModel.addAttribute("adminid",sessionObj.getAttribute("adminid").toString());
			}catch(NullPointerException e){
				pModel.addAttribute("adminid",0);
			}
		return "performances/competitions.jsp";
	}

	@RequestMapping(value = "/stats", method = RequestMethod.GET)
	public String stats(
			final ModelMap pModel,
			HttpSession sessionObj,
			@RequestParam(value = "cid", required = false) final String competition_id) {

		HashMap<String, List<Clubs>> c = ClubController.getSorted("region");
		Object[] tmp = c.keySet().toArray();
		String[] a = Arrays.copyOf(tmp, tmp.length, String[].class);
		Boolean b = true;

		if (competition_id == null) {
			b = false;
		}

		HashMap<String, VisualPerformance> vals = new HashMap<>();
		List<Performances> perfs = null;
		for (int i = 0; i < a.length; i++) {
			VisualPerformance p = new VisualPerformance();
			if (b) {
				perfs = PerformancesManager
						.getPerformancesByRegionAndCompetition(a[i],
								Integer.parseInt(competition_id));
			} else {
				perfs = PerformancesManager.getPerformancesByRegion(a[i]);
			}
			if (perfs.size() > 0) {
				Performances pbest = perfs.get(0);
				Members m = MembersManager.getMember(pbest.getMember_id());
				p.member_name = m.getFirstname() + " " + m.getLastname();
				p.competition = CompetitionsManager.getCompetitionName(pbest
						.getCompetition_id());
				p.club_name = ClubsManager.getClubName(m.getClubId());
				p.points = String.valueOf(pbest.getPoints());
				vals.put(a[i], p);
			}
		}

		pModel.addAttribute("stats", vals);
		try{
			pModel.addAttribute("adminid",sessionObj.getAttribute("adminid").toString());
			}catch(NullPointerException e){
				pModel.addAttribute("adminid",0);
			}
		try {
			pModel.addAttribute("sessid", sessionObj.getAttribute("uid")
					.toString());
		} catch (Exception e) {
			// nothing to do, user not connected.
		}
		return "performances/stats_region.jsp";

	}
}
