package fr.larget.utils;
/**
 * Objet utilis� pour la vue pour manipuler plus simplement des membres/points/competitions/clubs
 * @author Henri L.
 *
 */
public class VisualPerformance {
	public String member_name, points, competition, club_name;

	@Override
	public String toString() {
		return "VisualPerformance [member_name=" + member_name + ", points="
				+ points + ", club_name=" + club_name + ", competition="
				+ competition + "]";
	}

	public String getMember_name() {
		return member_name;
	}

	public void setMember_name(String member_name) {
		this.member_name = member_name;
	}

	public String getPoints() {
		return points;
		
	}

	public void setPoints(String points) {
		this.points = points;
	}

	public String getClub_name() {
		return club_name;
	}

	public void setClub_name(String club_name) {
		this.club_name = club_name;
	}

	public String getCompetition() {
		return competition;
	}

}
