package fr.larget.utils;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.core.io.DefaultResourceLoader;
import org.springframework.core.io.Resource;
import org.springframework.mail.MailSender;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
/**
 * envoi de mail au format HTML
 * @author Henri L.
 *
 */
public class HTMLMail {
	private JavaMailSender mailSender;

	public HTMLMail() {

	}

	public void setMailSender(JavaMailSender mailSender) {
		this.mailSender = mailSender;
	}

	public static void sendHTML(String to, String subject, String msg) {
		ApplicationContext context = new ClassPathXmlApplicationContext(
				"Spring-Mail.xml");

		HTMLMail mm = (HTMLMail) context.getBean("htmlMail");
		mm.sendMail(to, subject, msg);
	}

	public void sendMail(String to, String subject, String msg) {
		try {

			MimeMessage message = mailSender.createMimeMessage();

			message.setSubject(subject);
			MimeMessageHelper helper;
			helper = new MimeMessageHelper(message, true);

			helper.setTo(to);
			helper.setText(msg, true);
			mailSender.send(message);
		} catch (MessagingException ex) {
		}
	}

}