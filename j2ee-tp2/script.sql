
--
-- Structure de la table `club`
--

CREATE TABLE IF NOT EXISTS `club` (
`cid` int(11) NOT NULL,
  `club_name` varchar(100) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `region` varchar(100) DEFAULT 'Autre',
  `referentid` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=latin1;

--
-- Contenu de la table `club`
--

INSERT INTO `club` (`cid`, `club_name`, `region`, `referentid`) VALUES
(5, 'La fleche jeumontoise', 'Nord Pas-de-calais', 0),
(6, 'Association center training', 'Nord Pas-de-calais', 0),
(7, 'Arkhelios', 'Nord Pas-de-calais', 0),
(8, 'Bubble bump lens', 'Nord Pas-de-calais', 0),
(9, 'Capoeira brasil lille', 'Nord Pas-de-calais', 0),
(10, 'Ecole d''aikido roubaix', 'Nord Pas-de-calais', 0),
(11, 'Poney-club de marck', 'Nord Pas-de-calais', 0),
(12, 'Poney club d''offekerque', 'Nord Pas-de-calais', 0),
(13, 'X-trem boxing club annezin', 'Nord pas-de-calais', 0),
(14, 'Full contact douai', 'Nord Pas-de-calais', 0),
(15, 'Boxe americaine et kick boxing', 'Nord Pas-de-calais', 0),
(16, 'Zen dojang', 'Rh�ne alpes', 0),
(17, 'Arts martiaux rive droite', 'Rh�ne alpes', 0),
(18, 'Cupidon tir � l''arc', 'Midi-Pyr�n�es', 0),
(19, 'Yokidalbi aikido epa ista albi', 'Midi-Pyr�n�es', 0),
(20, 'O ski passion', 'Midi-Pyr�n�es', 0),
(21, 'Ecole de ski esprit montagne', 'Midi-Pyr�n�es', 4),
(22, 'Twinner sport- ski service', 'Midi-Pyr�n�es', 0),
(23, 'S.i.g.a.s.', 'Midi-Pyr�n�es', 6);

-- --------------------------------------------------------

--
-- Structure de la table `competition`
--

CREATE TABLE IF NOT EXISTS `competition` (
`compid` int(11) NOT NULL,
  `competition_label` varchar(255) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Contenu de la table `competition`
--

INSERT INTO `competition` (`compid`, `competition_label`) VALUES
(1, '450m cible mouvement');

-- --------------------------------------------------------

--
-- Structure de la table `member`
--

CREATE TABLE IF NOT EXISTS `member` (
`mid` int(11) NOT NULL,
  `firstname` varchar(100) NOT NULL,
  `lastname` varchar(100) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(300) DEFAULT NULL,
  `date_of_birth` date NOT NULL,
  `cid` int(11) NOT NULL,
  `accepted` int(1) NOT NULL DEFAULT '0',
  `date_add` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

--
-- Contenu de la table `member`
--

INSERT INTO `member` (`mid`, `firstname`, `lastname`, `email`, `password`, `date_of_birth`, `cid`, `accepted`, `date_add`) VALUES
(4, 'Henri', 'LARGET', 'h.larget@gmail.com', 'df61830ceab6f8b32b08d922787cd042', '1991-12-30', 21, 1, '2015-06-11 16:59:28');

-- --------------------------------------------------------

--
-- Structure de la table `performance`
--

CREATE TABLE IF NOT EXISTS `performance` (
`pid` int(11) NOT NULL,
  `compid` int(11) NOT NULL DEFAULT '0',
  `mid` int(11) NOT NULL,
  `points` int(11) NOT NULL,
  `date_add` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Contenu de la table `performance`
--

INSERT INTO `performance` (`pid`, `compid`, `mid`, `points`, `date_add`) VALUES
(1, 1, 4, 200, '2015-06-12 18:35:41');

--
-- Index pour les tables export�es
--

--
-- Index pour la table `club`
--
ALTER TABLE `club`
 ADD PRIMARY KEY (`cid`);

--
-- Index pour la table `competition`
--
ALTER TABLE `competition`
 ADD PRIMARY KEY (`compid`);

--
-- Index pour la table `member`
--
ALTER TABLE `member`
 ADD PRIMARY KEY (`mid`), ADD UNIQUE KEY `email` (`email`);

--
-- Index pour la table `performance`
--
ALTER TABLE `performance`
 ADD PRIMARY KEY (`pid`);

--
-- AUTO_INCREMENT pour les tables export�es
--

--
-- AUTO_INCREMENT pour la table `club`
--
ALTER TABLE `club`
MODIFY `cid` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=24;
--
-- AUTO_INCREMENT pour la table `competition`
--
ALTER TABLE `competition`
MODIFY `compid` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=1;
--
-- AUTO_INCREMENT pour la table `member`
--
ALTER TABLE `member`
MODIFY `mid` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=1;
--
-- AUTO_INCREMENT pour la table `performance`
--
ALTER TABLE `performance`
MODIFY `pid` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=1;