# Projet J2EE-TP2 #

## Installation ##
### Création de la base de données ###
script disponible dans jee-pj2-larget/script.sql
### Modification des fichiers de configuration ###
Dans le dossier resources, modifier Hibernate.cfg.xml pour modifier les accès base de données
Dans le dossier resources, modifier Spring-Mail.xml pour permettre l'envoi de mails.
### identifiants ###
membre : h.larget@gmail.com (mdp : 750p84ze)N16 )
mdp admin : pz33Qthe

## fonctionnalités manquantes ##
- mettre un commentaire quand on refuse un membre
- enregistrement dans la base de "comptes" administrateurs (pour le moment un simple mot de passe dans les sources)

## Liens utiles ##

[gdufrene.github.io/java_ee_spring-14/JEE_session2.html](http://gdufrene.github.io/java_ee_spring-14/JEE_session2.html)

## informations sur le contenu ##

les images utilisées pour le site ne m’appartiennent pas et je n'ai pas demandé d'autorisation auprès du propriétaire vu qu'elles restent dans un cadre strictement scolaire et sans but lucratif.